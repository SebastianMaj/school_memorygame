﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	// Keep an instance of GameManager
	public static GameManager instance;

	// All cards in the game
	private List<Card> _cards = new List<Card>();
	// Cards that are currently flipped
	[HideInInspector]
	public List<Card> flippedCards = new List<Card>();
	// Check if there is a card in the process of flipping
	[HideInInspector]
	public bool bIsCardFlipping = false;
	// Points UI
	public TextMeshProUGUI pointsText;
	// The prefab for the card
	public GameObject cardPrefab;
	// Card Container
	public Transform cardContainer;
	// Array of front sprites
	public Sprite backFace;
	public Sprite[] frontFaces = new Sprite[10];
	// Text Timer
	public TextMeshProUGUI timerText;
	
	// Points variable + callback
	private float _points = 0;
	public float points
	{
		get { return _points; }
		set { 
			_points = value;
			pointsText.text = string.Format("Score: {0}", _points);
		}
	}
	
	// Timer
	private Stopwatch _timer = new Stopwatch();

	// Keep track of card counts
	private int _totalCards;
	private int _totalUniqueCards {get { return _totalCards / 2; }}

	// Called on Awake
	private void Awake()
	{
		instance = this;
	}

	// Start the game with an amount of cards
	public void InitiateGame(int totalCards)
	{
		// Reset Game
		ResetGame();
		// Reset points
		points = 1000;
		// Start stopwatch
		_timer.Start();
		_timer.Restart();
		
		// Set the total cards value
		_totalCards = totalCards;
		
		// The current index for which front card is being spawned
		int curUniqueIndex = -1;
		for (int i = 0; i < _totalCards; i++)
		{
			// If the index is even start the next unique card (makes sure there is only 2 of each)
			if (i % 2 == 0) curUniqueIndex++;
			
			// Create card and place it in the container
			var card = GameObject.Instantiate(cardPrefab);
			var cardComp = card.GetComponent<Card>().Initiate(i, curUniqueIndex);
			card.transform.SetParent(cardContainer);
			
			// Reset Transform
			card.transform.localPosition = Vector3.zero;
			card.transform.localScale = Vector3.one;
			// Add card to the list
			_cards.Add(cardComp);
		}
		
		// Shuffle Cards
		for (int i = 0; i < cardContainer.childCount; i++)
		{
			cardContainer.transform.GetChild(i).SetSiblingIndex(Random.Range(0,cardContainer.childCount));
		}
	}

	// Called every frame
	private void Update()
	{
		if (_timer.IsRunning)
		{
			timerText.text = $"{_timer.Elapsed.Minutes}:{_timer.Elapsed.Seconds}";
		}
	}

	// Restarts the game over
	public void RestartGame()
	{
		// Only restart if a card is not flipping right now
		if(bIsCardFlipping) return;
		InitiateGame(_totalCards);
	}

	// Reset the game before starting a new one
	private void ResetGame()
	{
		// Clear the cards
		foreach (var card in _cards)
		{
			Destroy(card.gameObject);
		}
		_cards.Clear();
		
		// Hide win panel if it's open
		MenuManager.instance.endPanel.SetActive(false);
	}

	// This is called when a new card was flipped
	public void CardFlipped()
	{
		// Reset card flipping flag
		bIsCardFlipping = false;
		
		
		// Check if cards need to be checked
		if (flippedCards.Count < 2) return;
		
		// Match
		if (flippedCards[0].frontIndex == flippedCards[1].frontIndex)
		{
			AudioManager.instance.PlaySound("match");
			
			// Set both to matched
			flippedCards[0].Matched();
			flippedCards[1].Matched();
			flippedCards.Clear();
			
			// Check if win
			if (_gameWon)
			{
				_timer.Stop();
				MenuManager.instance.ShowEnd(true, _timer);
			}
		}
		// Not Match
		else
		{
			AudioManager.instance.PlaySound("nomatch");
			
			// Take away points
			points -= 40;
				
			// Flip cards back over
			flippedCards[0].FlipBack();
			flippedCards[1].FlipBack();
				
			// Clear flipped list
			flippedCards.Clear();
			
			// Check if loss
			if (points <= 0)
			{
				_timer.Stop();
				MenuManager.instance.ShowEnd(false, _timer);
			}
		}
	}

	// A boolean to check if all cards have been matched
	private bool _gameWon
	{
		get
		{
			foreach (var card in _cards)
			{
				if (!card.isMatched)
					return false;
			}

			return true;
		}
	}

	// Called when game is over or object destroyed
	private void OnDestroy()
	{
		_timer.Stop();
	}
}
