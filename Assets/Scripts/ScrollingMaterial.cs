﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingMaterial : MonoBehaviour
{
	// The material to use the offset on
	private Material _material;
	
	// How fast the offset on the material will move per second 
	public float scrollSpeed = 1.0f;
	// Save the offset values
	private float _currentXOffset = 0f;
	private float _currentYOffset = 0f;
	
	private void Awake()
	{
		// Get the material from the mesh renderer
		MeshRenderer meshRenderer = null;
		if ((meshRenderer = GetComponent<MeshRenderer>()) != null)
		{
			_material = meshRenderer.material;

			_currentXOffset = _material.GetTextureOffset("_MainTex").x;
			_currentYOffset = _material.GetTextureOffset("_MainTex").y;
		}
		else
			Destroy(this);
	}

	private void Update()
	{
		// Apply offset change to the material
		_currentXOffset += (scrollSpeed * Time.deltaTime);
		_material.SetTextureOffset("_MainTex", new Vector2(_currentXOffset, _currentYOffset));
	}
}
