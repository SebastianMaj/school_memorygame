﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
	// Keep an instance
	public static AudioManager instance;

	// The Audio Source component on the object
	private AudioSource _audioSource;
	// List of sounds in the game
	public List<Sound> sounds = new List<Sound>();

	// Called upon Awake
	private void Awake()
	{
		instance = this;
		_audioSource = GetComponent<AudioSource>();
	}

	// Plays a sound by it's name
	public void PlaySound(string soundName)
	{
		var sound = sounds.FirstOrDefault(x => x.name == soundName);
		if (sound.sound == null)
			return;
		_audioSource.PlayOneShot(sound.sound);
	}
}

// Struct to hold name and sound
[System.Serializable]
public struct Sound
{
	public string name;
	public AudioClip sound;
}
