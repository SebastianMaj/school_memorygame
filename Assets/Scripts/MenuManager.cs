﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class MenuManager : MonoBehaviour
{
    // Keep a copy of an instance
    public static MenuManager instance;
    
    // Store Main Menu Container
    public GameObject mainMenuContainer;
    // Store game Container
    public GameObject gameContainer;
    // Difficulty List
    public Color difficultyUnselectColor, difficultySelectColor;
    public List<Difficulty> difficulties = new List<Difficulty>();
    private int _currentDifficulty;
    public int currentDifficulty {
        get { return _currentDifficulty; }
        set { _currentDifficulty = value; UpdateDifficulty(); }
    }

    [Space]

    // Store end panel
    public GameObject endPanel;
    public TextMeshProUGUI endTextResult;
    public TextMeshProUGUI endTextTimer;
    public TextMeshProUGUI endTextScore;
    public Color endWinColor;
    public Color endLoseColor;
        
    [Space]
    
    // This is used for transitions
    public Image loadingPanel;

    // This is called upon Awake
    private void Awake()
    {
        instance = this;
        
        mainMenuContainer.SetActive(true);
        gameContainer.SetActive(false);
        currentDifficulty = difficulties[0].cardsAmount;
    }

    // Starts the game
    public void PlayGame()
    {
        print("Starting game");
        GameManager.instance.InitiateGame(currentDifficulty);
        StartCoroutine(ShowTransition());
    }

    // Quits the game
    public void ExitGame()
    {
        print("Exiting game");
        
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
    }
    
    // Small animation used for transitions
    private IEnumerator ShowTransition(bool inToGame = true)
    {
        const float timer = 1.0f;
        loadingPanel.DOFillAmount(1, timer);
        yield return new WaitForSeconds(timer);

        mainMenuContainer.SetActive(!inToGame);
        gameContainer.SetActive(inToGame);
        
        yield return new WaitForSeconds(1);
        loadingPanel.DOFillAmount(0, timer);
    }
    
    // Show Win or Loss
    public void ShowEnd(bool win, Stopwatch timer)
    {
        // Show end panel
        endPanel.SetActive(true);
        
        // Win / Lose Text
        if (win)
        {
            AudioManager.instance.PlaySound("win");
            endTextResult.text = "You Won!";
            endTextResult.color = endWinColor;
        }
        else
        {
            AudioManager.instance.PlaySound("loss");
            endTextResult.text = "You lost.";
            endTextResult.color = endLoseColor;
        }
        
        // Remaining text
        endTextTimer.text = $"Time: {timer.Elapsed.Minutes}:{timer.Elapsed.Seconds}";
        endTextScore.text = $"Score: {GameManager.instance.points}";
    }

    // Sets the difficulty
    public void SetDifficulty(int value)
    {
        currentDifficulty = value;
    }
    
    // Update the difficulty of the game
    public void UpdateDifficulty()
    {
        foreach (var difficulty in difficulties)
            difficulty.button.color = difficultyUnselectColor;

        difficulties.FirstOrDefault(x => x.cardsAmount == currentDifficulty).button.color = difficultySelectColor;
    }

    // Go back to main menu
    public void GoToMainMenu()
    {
        StartCoroutine(ShowTransition(false));
    }
}

[System.Serializable]
public struct Difficulty
{
    public int cardsAmount;
    public Image button;
}