﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Card : MonoBehaviour
{
	// The cards image
	private Image _image;
	// Track if it's facing front or back
	private bool _isFront = false;

	// This determines whether the card was matched or not
	public bool isMatched = false;
	// The card index on the board
	public int cardIndex = 0;
	// The index of the symbol on the front
	public int frontIndex = 0;

	// Constructor to save index
	public Card Initiate(int cardIndex, int frontIndex)
	{
		_image = GetComponent<Image>();
		this.cardIndex = cardIndex;
		this.frontIndex = frontIndex;
		return this;
	}
	
	// Flips the card over
	public void FlipFront()
	{
		// Check if there are 2 cards flipped or a card is currently being flipped
		if (GameManager.instance.flippedCards.Count >= 2 || GameManager.instance.bIsCardFlipping) return;
		// Make sure that you don't reflip a currently flipped card
		if (_isFront || isMatched) return;

		GameManager.instance.bIsCardFlipping = true;
		AudioManager.instance.PlaySound("flip");
		
		// Make is front true + Animate and add to Gamemanager list
		_isFront = true;
		StartCoroutine(FlipAnimation());
		GameManager.instance.flippedCards.Add(this);
	}
	
	// Flips the card back
	public void FlipBack()
	{
		// Make sure that you don't flip back a card pair that is already matched
		if (isMatched) return;
		
		GameManager.instance.bIsCardFlipping = true;
		
		// Make is front false and animate
		_isFront = false;
		StartCoroutine(FlipAnimation(false));
	}

	// This handles the triggers for when it's matched
	public void Matched()
	{
		isMatched = true;
		var fadedColor = _image.color;
		fadedColor.a = 0;
		_image.DOColor(fadedColor, 1.0f);
	}

	// This is for the card flip animation
	private IEnumerator FlipAnimation(bool front = true)
	{
		// Animate scale on x axis to 0 in 0.5 seconds
		transform.DOScaleX(0, 0.25f);
		
		// Wait 0.8 seconds then set the image sprite to the front face
		yield return new WaitForSeconds(0.4f);
		_image.sprite = front ? GameManager.instance.frontFaces[frontIndex] : GameManager.instance.backFace;
		
		// Animate scale on x axis to 1 in 0.5 seconds
		transform.DOScale(1, 0.25f);
		yield return new WaitForSeconds(0.4f);
		
		// Tell GameManager that the card was flipped
		GameManager.instance.CardFlipped();
	}
}
